package com.emsav.howlonguntil;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HolidayFragment extends Fragment {
    ArrayAdapter<String> holListNames;
    TextView year;
    Toast toast;
    int screenSize;
    FragmentTransaction fragmentTransaction;
    ListView holList;
    EventFragment event;

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        holList.setChoiceMode(
                activateOnItemClick ? ListView.CHOICE_MODE_SINGLE
                        : ListView.CHOICE_MODE_NONE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        final View view = inflater.inflate(R.layout.fragment_holiday, container, false);

        HolidayCalendar obs = new HolidayCalendar();
        ArrayList<List<String>> hols = new ArrayList<>();
        for(List<String> holiday : obs.holidays){
            hols.add(holiday);
        }

        ArrayList<String> names = new ArrayList<>();
        for(List<String> nm : hols){
            names.add(nm.get(4));
        }

        holListNames = new ArrayAdapter(getActivity().getApplicationContext(), R.layout.custom_simple_item, names);
        final ArrayAdapter<List<String>> hoListObs = new ArrayAdapter(getActivity().getApplicationContext(), R.layout.custom_simple_item, hols);
        holList = (ListView) view.findViewById(R.id.listHolidayEvents);
        holList.setAdapter(holListNames);
        year = (TextView) view.findViewById(R.id.yearEditText);

        holList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List<String> d = hoListObs.getItem(position);
                HolidayCalendar hc = new HolidayCalendar();
                Calendar t = Calendar.getInstance();

                //check for older than today dates, empty
                if (year.getText().toString().isEmpty()) {
                    toast = toast.makeText(getActivity().getApplicationContext(), getActivity().getApplicationContext().getString(R.string.empty_string), Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }else {
                    int yearText = Integer.parseInt(year.getText().toString());
                    Calendar xx = hc.calcHoliday(hc.mapMaker(d), yearText);
                    if (EventFragment.isEventToday(t, xx)) {
                        toast = toast.makeText(getActivity().getApplicationContext(), getActivity().getApplicationContext().getString(R.string.thats_today_text), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    } else if (EventFragment.isEventBeforeToday(t, xx)) {
                        toast = toast.makeText(getActivity().getApplicationContext(), getActivity().getApplicationContext().getString(R.string.thats_before_today_text), Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show();
                    }else {
                        if(screenSize>= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                           if(event!=null){
                               fragmentTransaction = getFragmentManager().beginTransaction();
                               getFragmentManager().popBackStack("hol", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                               fragmentTransaction.remove(event);
                               fragmentTransaction.commit();
                           }
                            event = EventFragment.newInstance(d.get(4),xx);

                            fragmentTransaction = getFragmentManager().beginTransaction();
                            fragmentTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out
                            );

                            fragmentTransaction.add(R.id.event_fragment_container, event)
                                    .addToBackStack("hol");
                            fragmentTransaction.commit();

                        }else {
                            Intent detailsView = new Intent(getActivity().getApplicationContext(), EventActivity.class);
                            detailsView.putExtra("event", xx);
                            detailsView.putExtra("name", d.get(4));
                            startActivity(detailsView);

                        }
                    }
                }
            }
        });

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


}
