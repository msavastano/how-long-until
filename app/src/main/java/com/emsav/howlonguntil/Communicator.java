package com.emsav.howlonguntil;

import java.util.Calendar;

/**
 * Created by michael on 1/6/15.
 */
public interface Communicator {
    public void respond(String data, Calendar event);
    public void str(String Str);
}
