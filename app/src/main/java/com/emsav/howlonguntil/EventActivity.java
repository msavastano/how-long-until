package com.emsav.howlonguntil;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Calendar;


public class EventActivity extends ActionBarActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Calendar cal = (Calendar) getIntent().getSerializableExtra("event");
        String na = (String) getIntent().getSerializableExtra("name");
        EventFragment event = EventFragment.newInstance(na, cal);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, event).addToBackStack(null)
                    //.setCustomAnimations(R.anim.slide_in, R.anim.slide_out,
                     //   R.anim.slide_in, R.anim.slide_out)
                    .commit();
        }
    }





}
