package com.emsav.howlonguntil;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Calendar;


public class SavedEventsListFragment extends Fragment {

    int screenSize;
    MyDBHandler dbHelper;
    Toast toast;
    FragmentTransaction fragmentTransaction;
    EventFragment event;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_saved_events_list, container, false);

        dbHelper = new MyDBHandler(getActivity().getApplicationContext(), null, null, 1);
        Cursor ed = dbHelper.getAllEventDates();
        String[] from = {"name"};
        int[] to = {R.id.customListItem};

        ListAdapter savedListAdapter=new SimpleCursorAdapter(getActivity().getApplicationContext(),
                R.layout.custom_simple_item, ed, from, to);
        ListView savedList = (ListView) view.findViewById(R.id.listViewSavedEvents);
        savedList.setAdapter(savedListAdapter);

        //On click go to event detail page
        savedList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cur = dbHelper.getDateByID(id);
                Cursor curName = dbHelper.getNameByID(id);
                cur.moveToFirst();
                curName.moveToFirst();
                String evdate = "";
                String evname = "";
                while (!cur.isAfterLast()) {
                    evdate = cur.getString(0);
                    cur.moveToNext();
                }
                while (!curName.isAfterLast()) {
                    evname = curName.getString(0);
                    curName.moveToNext();
                }

                Calendar t = Calendar.getInstance();

                Long unixtime = Long.parseLong(evdate);
                Calendar unix2Cal = Calendar.getInstance();
                unix2Cal.setTimeInMillis(unixtime);

                //if event has passed or is 'today'. do not go to event detail page
                if (EventFragment.isEventToday(t, unix2Cal)) {
                    toast = toast.makeText(getActivity().getApplicationContext(), "Today!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                } else if (EventFragment.isEventBeforeToday(t, unix2Cal)) {
                    toast = toast.makeText(getActivity().getApplicationContext(), "Past", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                } else {

                    if(screenSize>= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                        if(event!=null){
                            fragmentTransaction = getFragmentManager().beginTransaction();
                            getFragmentManager().popBackStack("saved", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            fragmentTransaction.remove(event);
                            fragmentTransaction.commit();
                        }
                        event = EventFragment.newInstance(evname, unix2Cal);
                        fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);

                        fragmentTransaction.add(R.id.event_fragment_container, event)
                                .addToBackStack("saved");
                        fragmentTransaction.commit();

                    }else {
                        Intent detailsView = new Intent(getActivity().getApplicationContext(), EventActivity.class);
                        detailsView.putExtra("event", unix2Cal);
                        detailsView.putExtra("name", evname);
                        startActivity(detailsView);

                    }
                }
            }
        });

        savedList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                dbHelper.deleteByID(id);
                Intent i = new Intent(getActivity().getApplicationContext(), SavedEventsActivity.class);
                startActivity(i);
                getActivity().finish();
                toast = toast.makeText(getActivity().getApplicationContext(), "Delete", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER,0,0);
                toast.show();
                return true;
            }
        });

        return view;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



}
