package com.emsav.howlonguntil;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.ads.AdSize;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.Calendar;


public class PickDateFragment extends Fragment {

    int screenSize;

    FragmentTransaction fragmentTransaction;
    DatePicker datePicker;
    Calendar today;
    Calendar eventDate;
    Toast toast;
    EventFragment event;

    public PickDateFragment() {
    }

    public static PickDateFragment newInstance(){
        PickDateFragment fragment = new PickDateFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }



        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        final View rootView;

        if( (getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE &&
                screenSize==Configuration.SCREENLAYOUT_SIZE_LARGE ) ||
                (getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT &&
                        screenSize==Configuration.SCREENLAYOUT_SIZE_NORMAL) ){
            rootView = inflater.inflate(R.layout.fragment_main, container, false);
        }else{
            rootView = inflater.inflate(R.layout.fragment_main_land, container, false);
        }

        datePicker = (DatePicker) rootView.findViewById(R.id.datePickerLayout);
        today = Calendar.getInstance();

        Button howLongButton = (Button) rootView.findViewById(R.id.howLongButton);
        howLongButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Gets values from datepicker view
                //Sets event to midnight

                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year =  datePicker.getYear();
                eventDate = Calendar.getInstance();
                eventDate.set(year, month, day);
                eventDate.set(Calendar.HOUR_OF_DAY, 0);
                eventDate.set(Calendar.MINUTE, 0);
                eventDate.set(Calendar.SECOND, 0);
                eventDate.set(Calendar.MILLISECOND, 0);

                //Starts activity, but not if date n picker is today or earlier
                if(EventFragment.isEventToday(today, eventDate)){
                    toast = Toast.makeText(getActivity().getApplicationContext(), getActivity().getApplicationContext().getString(R.string.thats_today_text),Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }else if(EventFragment.isEventBeforeToday(today,eventDate)){
                    toast = Toast.makeText(getActivity().getApplicationContext(), getActivity().getApplicationContext().getString(R.string.thats_before_today_text),Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }else {

                    if (screenSize >= Configuration.SCREENLAYOUT_SIZE_LARGE) {
                        if(event!=null){
                            fragmentTransaction = getFragmentManager().beginTransaction();
                            getFragmentManager().popBackStack("pick", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            fragmentTransaction.remove(event);
                            fragmentTransaction.commit();
                        }
                        event = EventFragment.newInstance("", eventDate);
                        fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);

                        fragmentTransaction.add(R.id.event_fragment_container, event);

                        fragmentTransaction.addToBackStack("pick");

                        fragmentTransaction.commit();
                    } else {
                        Intent i = new Intent(getActivity(), EventActivity.class);
                        i.putExtra("event", eventDate);
                        i.putExtra("name", "");
                        startActivity(i);
                    }
                }
            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
